import React from "react"
import { Button } from "react-bootstrap"
import { ArrowRight } from "react-bootstrap-icons"

const Card = props => {
  return (
    <div className="CustomCard">
      <h3 className="CardTitle">{props.title}</h3>
      <p className="CardValueAll">
        {props.currency}{" "}
        <span className="CardValue">{props.pricePerMonth}</span> /mo
      </p>
      {props.children}
      <p className="CardDesc">{props.description}</p>
      <ul className="CardList">
        {props.li.map((el, i) => (
          <li key={i}>
            <ArrowRight />
            <span className="CardLi">{el}</span>
          </li>
        ))}
      </ul>
      <Button variant="primary" className="ButtonSelect">
        Select
      </Button>
    </div>
  )
}

export default Card
